import { NextPage } from "next";



const Header: NextPage = () => {
  return (
    <div>
      <ul className="navBar">
        <li>
          Home
        </li>
        <li>
          Vencimientos
        </li>
        <li>
          Facturas
        </li>
      </ul>
      <hr />
    </div>
  );
};

export default Header;
