import "../styles/globals.css";
import type { AppProps } from "next/app";
import Header from "./header";
import Footer from "./footer";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <div style={{padding:"15px"}}>
      <Header />
      <Component {...pageProps} />
      <Footer />
    </div>
  );
}

export default MyApp;
