import type { NextPage } from "next";
import { useState } from "react";

const Boton = ({ texto, onClick }: any) => (
  <button onClick={onClick}>{texto}</button>
);

const IngresoAlBalanceInput = ({ setIngreso, onClick }: any) => {
  return (
    <div>
      <input type="number" onChange={(e: any) => setIngreso(e.target.value)} />
      <Boton texto="Sumar ingreso" onClick={onClick} />
    </div>
  );
};

const GastoAlBalanceInput = ({ setGasto, onClick }: any) => {
  return (
    <div>
      <input type="number" onChange={(e: any) => setGasto(e.target.value)} />
      <Boton texto="Restar gasto" onClick={onClick} />
    </div>
  );
};

const AhorroDelBalanceInput = ({ setIngresoAhorro, onClick }: any) => {
  return (
    <div>
      <input
        type="number"
        onChange={(e: any) => setIngresoAhorro(parseInt(e.target.value))}
      />
      <Boton texto="Sumar ahorro" onClick={onClick} />
    </div>
  );
};

const Balance = () => {
  const [ingreso, setIngreso]: any = useState();
  const [gasto, setGasto]: any = useState();
  const [presupuesto, setPresupuesto] = useState(0);

  const [visibilidad, modificarVisibilidad] = useState(false);
  const [mostrarInputGasto, modificarMostrarInputGasto] = useState(false);
  const [mostrarInputAhorro, modificarMostrarInputAhorro] = useState(false);

  const [ingresoAhorro, setIngresoAhorro] = useState(0);
  const [ahorro, setAhorro] = useState(0);

  return (
    <>
      <h2>Balance</h2>
      <p>Saldo en pesos ${presupuesto}</p>
      <p>Saldo en dolares ${(presupuesto / 331).toFixed(2)}</p>
      <div>
        <Boton
          texto="Agregar ingreso"
          onClick={() => modificarVisibilidad((invisible) => !invisible)}
        />
        <Boton
          texto="Agregar gasto"
          onClick={() => modificarMostrarInputGasto((invisible) => !invisible)}
        />
      </div>
      <div>
        <Boton
          texto="Agregar ahorro"
          onClick={() => modificarMostrarInputAhorro((invisible) => !invisible)}
        />
      </div>
      <div>
        <h2>Ahorros</h2>
        <p>Total: ${ahorro}</p>
        <p>Esperado: </p>
      </div>

      {visibilidad ? (
        <IngresoAlBalanceInput
          setIngreso={setIngreso}
          onClick={() => {
            ingreso >= 0
              ? setPresupuesto(presupuesto + parseInt(ingreso))
              : alert("No se pueden ingresar nros negativos");
          }}
        />
      ) : null}

      {mostrarInputGasto ? (
        <GastoAlBalanceInput
          setGasto={setGasto}
          onClick={() => {
            gasto <= 0
              ? setPresupuesto(presupuesto + parseInt(gasto))
              : alert("No se pueden ingresar nros positivos");
          }}
        />
      ) : null}

      {mostrarInputAhorro ? (
        <AhorroDelBalanceInput
          setIngresoAhorro={setIngresoAhorro}
          onClick={() => {
            if (presupuesto === 0) {
              alert("El presupuesto esta en 0");
            } else if (ingresoAhorro <= 0) {
              alert("No se puede ingresar un nro menor o igual a 0");
            } else {
              setAhorro(ingresoAhorro);
              setPresupuesto(presupuesto - ingresoAhorro);
              setAhorro(ahorro + ingresoAhorro);
            }
          }}
        />
      ) : null}
      <hr />
    </>
  );
};

/* const GastosRealizados: React.FC<{
  concepto: string;
  importe: number;
}> = ({ concepto, importe }) => {
  return (
    <>
      <h3>{concepto}</h3>
      <p>${importe}</p>
      <hr />
    </>
  );
};
 */
const Home: NextPage = () => {
  return (
    <div>
      <Balance />
    </div>
  );
};

export default Home;
