const errores = {
  noPuedeIngresarMontoMenorAUno: "El monto a ingresar debe ser mayor a 0",
};

const billetera = {
  agregarIngreso: (monto: number) => {
    if (monto <= 0) throw new Error(errores.noPuedeIngresarMontoMenorAUno);
  },
  /* ,agregarGasto:(monto:number)=>{
        if(monto < 0){
            throw new Error("El monto a ingresar debe ser mayor a 0")
        }else if(monto == 0){
            throw new Error("No puede ingresar 0")
        }
    } */
};

export default billetera;
