# App de finanzas

## Descripcion del problema

Quiero poder trackear mis finanzas, teniendo en cuenta ingresos, egresos y ahorros.

## Cosas que deberia poder hacer:

- Ver balance
- agregar ingreso al balance
- agregar egreso al balance
- destinar parte del balance a ahorros


## Diagramas

### Casos de uso

```mermaid

graph LR
    user((Usuario))---casoCrearIngreso(crear ingreso)
    casoCrearIngreso
    user
        ---casoCrearEgreso(crear egreso)
    casoCrearEgreso
    user
        ---casoVerBalance(ver balance)
    user
        ---casoAgregarAhorro(agregar ahorro)
    casoAgregarAhorro
    user
        ---casoVerHistorial(ver historial de movimientos)
