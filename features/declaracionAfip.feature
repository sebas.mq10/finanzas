# language: es

Característica: Necesito hacer una declaracion a la AFIP en base a un plan de facturas

    Necesito que las facturas emitidas sean enviadas y aprobadas por la AFIP

Escenario: Las facturas son enviadas electrónicamente a AFIP para ser aprobadas
    Dado que tenemos una factura ya emitida
    Cuando se envie al sistema de AFIP
    Entonces la factura será aprobada
