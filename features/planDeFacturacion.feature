# language: es

Característica: Producir un plan de facturación para el monto total todavia no declarado

    Necesito generar facturas que como mínimo igualen la suma de ingresos bancarizados

Escenario: Generar facturas del último período semanal completo
    Dado el monto total de los ingresos no declarados de última semana finalizada
    Cuando el total es mayor a $0
    Entonces genero una factura que sea igual al total de ingresos no declarados

Escenario: Emitir una factura a consumidor final sin identificación
    Dado el total de los ingresos no declarados de la semana
    Cuando veo que el total es mayor al tope por factura sin identificación
    Entonces emito 2 o más facturas que cubran el total de los ingresos no declarados

Esquema del escenario: Emitir facturas de una semana 
    Dado un ingreso bancarizado de <ingresos>
    Y siendo $60000 el tope para facturar a consumidor final sin identificar al cliente
    Cuando genero la facturación del período
    Entonces se emiten <facturas> de <importe>

    Ejemplos:
        | ingresos | facturas | importe |
        | 80000    |    2     |  40000  |
        | 100000   |    2     |  50000  |
        | 180000   |    3     |  60000  |

