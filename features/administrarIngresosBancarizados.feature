# language: es

Característica: Administrar ingresos bancarizados

    Necesito justificar, ingresar y ver todos los movimientos bancarios

Escenario: Registrar una venta de USDT por binance
    Dado un listado de ingresos bancarizados vacío
    Y una cotización en binance de $360
    Cuando vendo 100 usdt por binance
    Entonces el último movimiento del registro de ingresos bancarizados es de $36000
    Y el saldo de ingresos bancarizados no declarados es de $36000

Escenario: El usuario deposita efectivo en su cuenta bancaria por un monto de $20000
    Dado un saldo de ingresos bancarizados no declarados de $30000
    Cuando el usuario deposita efectivo en su cuenta por un monto de $20000
    Entonces el nuevo saldo de ingresos bancarizados no declarados es de $50000

Escenario: Ver el listado de movimientos
    Dado un listado de ingresos bancarizados con al menos un movimiento
    Cuando quiero consultar los movimientos
    Entonces veo los movimientos
  





