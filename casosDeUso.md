# Casos de uso App Finanzas

- Debe poder verse el Balance
- Agregar un gasto, especificando monto,tipo de gasto y fecha
- El gasto se debe descontar del balance

- Agregar ingresos, especificando monto,tipo de ingreso y fecha
- El ingreso se debe sumar al balance

- Debe poder enviar dinero del Balance a ahorros
- Añadir cuentas, ver historial de las mismas
- Debe poder elegir la divisa
- Debe tener recordatorios
- Debe tener sistema de login